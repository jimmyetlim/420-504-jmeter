package ca.claurendeau.jmeter;

public class MyProcessE implements MyProcess{

    @Override
    public String doSomething() {
        MyProcess.sleep(200);
        return "Process E has done something";
    }

}
