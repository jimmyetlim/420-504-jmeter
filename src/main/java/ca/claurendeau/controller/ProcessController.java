package ca.claurendeau.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import ca.claurendeau.jmeter.MyProcess;

@RestController
public class ProcessController {

    @GetMapping("/process")
    public String doProcess() {
        return MyProcess.createRandomProcess().doSomething();
    }
}
