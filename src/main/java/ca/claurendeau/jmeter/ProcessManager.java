package ca.claurendeau.jmeter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class ProcessManager {
    
    private List<MyProcess> myProcesses = new ArrayList<>();

    public void createAndRunProcesses(int numberOfProcesses) {
        IntStream.range(0, numberOfProcesses)
                 .forEach(nbr -> {
                     myProcesses.add(MyProcess.createRandomProcess());             
                 });
        while (true) {
            for (MyProcess myProcess : myProcesses) {
                System.out.println(myProcess.doSomething());
            }
        }
        
    }
    
}
