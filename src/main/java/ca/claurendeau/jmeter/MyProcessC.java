package ca.claurendeau.jmeter;

public class MyProcessC implements MyProcess{

    @Override
    public String doSomething() {
        MyProcess.sleep(400);
        return "Process C has done something";
    }

}
