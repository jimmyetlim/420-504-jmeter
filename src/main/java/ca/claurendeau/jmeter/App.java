package ca.claurendeau.jmeter;

public class App {
    public static void main(String[] args) {
        ProcessManager processManager = new ProcessManager();
        processManager.createAndRunProcesses(20);
    }
}
